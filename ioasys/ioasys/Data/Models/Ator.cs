﻿namespace ioasys.Data.Models
{
    public class Ator
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public Filme Filme { get; set; }
    }
}
