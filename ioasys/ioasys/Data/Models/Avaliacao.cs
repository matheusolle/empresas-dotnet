﻿namespace ioasys.Data.Models
{
    public class Avaliacao
    {
        public int Id { get; set; }
        public Usuario Usuario { get; set; }
        public Filme Filme { get; set; }
        public Nota Nota { get; set; }
    }

    public enum Nota
    {
        Péssimo = 0,
        Ruim = 1,
        Regular = 2,
        Bom = 3,
        Ótimo = 4
    }
}
