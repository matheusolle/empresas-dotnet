﻿namespace ioasys.Data.Models
{
    public class Genero
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public Filme Filme { get; set; }
    }
}
