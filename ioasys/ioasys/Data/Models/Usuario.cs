﻿namespace ioasys.Data.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public TipoDeUsuario Administrador { get; set; }
        public bool Ativo { get; set; }

        //ToDo

        //Bio
        //UserId
        //Gender
        //Birthday MM-DD-YYYY
        //Country/Region
    }
    public enum TipoDeUsuario
    {
        Administrador = 1,
        Usuario = 0,
    }
}
