﻿using ioasys.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ioasys.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

        public DbSet<Avaliacao> Avaliacoes { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Filme> Filmes { get; set; }
        public DbSet<Ator> Atores { get; set; }
        public DbSet<Diretor> Diretores { get; set; }
        public DbSet<Genero> Generos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Avaliacao>();
            modelBuilder.Entity<Usuario>();
            modelBuilder.Entity<Filme>();
            modelBuilder.Entity<Ator>();
            modelBuilder.Entity<Diretor>();
            modelBuilder.Entity<Genero>();
        }
    }
}
