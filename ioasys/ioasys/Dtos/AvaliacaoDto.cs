﻿using ioasys.Data.Models;

namespace ioasys.Dtos
{
    public class AvaliacaoDto
    {
        public int UsuarioId { get; set; }
        public int FilmeId { get; set; }
        public Nota Nota { get; set; }
    }
}
