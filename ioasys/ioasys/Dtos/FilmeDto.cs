﻿#nullable enable
using System.Collections.Generic;

namespace ioasys.Dtos
{
    public class FilmeDto
    {
        public string? Nome { get; set; }
        public List<string>? Atores { get; set; }
        public List<string>? Diretores { get; set; }
        public List<string>? Generos { get; set; }
        public decimal? Nota { get; set; }
        public int? Pagina { get; set; }
        public int? ItensPagina { get; set; }
    }
}