﻿using ioasys.Data;
using ioasys.Data.Models;
using ioasys.Models.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace ioasys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly DatabaseContext _context;
        private readonly IConfiguration _configuration;

        public LoginController(DatabaseContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult PostLogin([FromBody] LoginDto request)
        {
            var usario = _context.Usuarios.Where(u => u.Email == request.Email && u.Senha == Criptografa(request.Senha)).FirstOrDefault();

            if (usario != null)
            {
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, usario.Nome),
                    new Claim(ClaimTypes.Email, usario.Email),
                    new Claim(ClaimTypes.Role, usario.Administrador == TipoDeUsuario.Administrador ? "Admin" : "User")
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SecurityKey"]));
                var credenciais = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: "ioasys",
                    audience: "ioasys",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(60),
                    signingCredentials: credenciais
                );

                return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
            }

            return BadRequest("Usuário ou senha inválido");
        }

        private string Criptografa(string senha) =>
            Convert.ToBase64String(Encoding.UTF8.GetBytes(senha + _configuration["SecurityKey"]));
    }
}
