﻿using ioasys.Data;
using ioasys.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ioasys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly DatabaseContext _context;
        private readonly IConfiguration _configuration;

        public UsuariosController(DatabaseContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Usuario>>> GetUsuarios(int? pagina, int? itensPagina)
        {
            var usuarios = _context.Usuarios.Where(u => u.Administrador == TipoDeUsuario.Usuario && u.Ativo).OrderBy(u => u.Nome);

            if (pagina > 0 && itensPagina > 0)
                return await usuarios.Skip((int)((pagina - 1) * itensPagina)).Take((int)itensPagina).ToListAsync();

            return await usuarios.ToListAsync();
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuario(int id, Usuario usuario)
        {
            if (id != usuario.Id) return BadRequest();

            usuario.Senha = Criptografa(usuario.Senha);
            _context.Entry(usuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id)) return NotFound("Usuário não existe");
                else throw;
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Usuario>> PostUsuario(Usuario usuario)
        {
            usuario.Senha = Criptografa(usuario.Senha);
            _context.Usuarios.Add(usuario);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUsuario", new { id = usuario.Id }, usuario);
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DisableUsuario(int id)
        {
            var usuario = await _context.Usuarios.FindAsync(id);

            if (usuario == null) return NotFound("Usuário não existe");

            usuario.Ativo = false;
            _context.Entry(usuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id)) return NotFound("Usuário não existe");
                else throw;
            }

            return NoContent();
        }

        private bool UsuarioExists(int id) =>
            _context.Usuarios.Any(e => e.Id == id);

        private string Criptografa(string senha) =>
            Convert.ToBase64String(Encoding.UTF8.GetBytes(senha + _configuration["SecurityKey"]));
    }
}
