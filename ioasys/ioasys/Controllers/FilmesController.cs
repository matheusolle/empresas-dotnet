﻿using ioasys.Data;
using ioasys.Data.Models;
using ioasys.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ioasys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilmesController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public FilmesController(DatabaseContext context)
        {
            _context = context;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Filme>>> GetFilmes(FilmeDto request)
        {
            IQueryable<Filme> filmes = _context.Filmes;

            if (!String.IsNullOrEmpty(request.Nome))
            {
                filmes = filmes.Where(f => f.Nome == request.Nome.ToUpper());
            }

            if (request.Generos != null)
            {
                filmes = from f in filmes
                         join g in _context.Generos
                         on f.Id equals g.Filme.Id
                         where request.Generos.Contains(g.Nome)
                         select f;
            }

            if (request.Diretores != null)
            {
                filmes = from f in filmes
                         join d in _context.Diretores
                         on f.Id equals d.Filme.Id
                         where request.Diretores.Contains(d.Nome)
                         select f;
            }

            if (request.Atores != null)
            {
                filmes = from f in filmes
                         join a in _context.Atores
                         on f.Id equals a.Filme.Id
                         where request.Atores.Contains(a.Nome)
                         select f;
            }

            if (request.Pagina > 0 && request.ItensPagina > 0)
            {
                filmes = filmes.Skip((int)((request.Pagina - 1) * request.ItensPagina)).Take((int)request.ItensPagina);
            }

            return await filmes.OrderBy(f => f.Nota).OrderBy(f => f.Nome).ToListAsync();
        }

        [Authorize(Roles = "User")]
        [HttpPost("rating")]
        public async Task<ActionResult> PostRating(AvaliacaoDto request)
        {
            var usuario = _context.Usuarios.FirstOrDefault(u => u.Id == request.UsuarioId);
            if (usuario == null) return NotFound("Usuário não existe");

            var filme = _context.Filmes.FirstOrDefault(u => u.Id == request.FilmeId);
            if (filme == null) return NotFound("Filme não existe");

            var avaliacao = new Avaliacao()
            {
                Usuario = usuario,
                Filme = filme,
                Nota = request.Nota
            };

            var avaliacoes = _context.Avaliacoes.Where(a => a.Filme == filme);
            if (avaliacoes.Count() > 1) filme.Nota += Convert.ToInt32(request.Nota + 1) / avaliacoes.Count();

            _context.Entry(filme).State = EntityState.Modified;
            _context.Avaliacoes.Add(avaliacao);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAvaliacao", new { id = avaliacao.Id }, avaliacao);
        }

        [Authorize]
        [HttpGet("filme")]
        public async Task<ActionResult<FilmeDto>> GetFilme(int id)
        {
            var filme = await _context.Filmes.FirstOrDefaultAsync(f => f.Id == id);
            if (filme == null) return NotFound("Filme não existe");

            var generos = _context.Generos.Where(g => g.Filme.Id == filme.Id);
            var diretores = _context.Diretores.Where(g => g.Filme.Id == filme.Id);
            var atores = _context.Atores.Where(g => g.Filme.Id == filme.Id);

            var filmeDto = new FilmeDto()
            {
                Nome = filme.Nome,
                Nota = filme.Nota,
            };

            if (generos.Any()) filmeDto.Generos = await generos.Select(g => g.Nome).ToListAsync();
            if (diretores.Any()) filmeDto.Diretores = await diretores.Select(g => g.Nome).ToListAsync();
            if (atores.Any()) filmeDto.Atores = await atores.Select(g => g.Nome).ToListAsync();

            return Ok(filmeDto);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<Filme>> PostFilme(FilmeDto request)
        {
            var filme = new Filme()
            {
                Nome = request.Nome,
            };

            var atores = new List<Ator>();
            var generos = new List<Genero>();
            var diretores = new List<Diretor>();

            foreach (string ator in request.Atores)
            {
                atores.Add(new Ator() { Filme = filme, Nome = ator });
            }

            foreach (string genero in request.Generos)
            {
                generos.Add(new Genero() { Filme = filme, Nome = genero });
            }

            foreach (string diretor in request.Diretores)
            {
                diretores.Add(new Diretor() { Filme = filme, Nome = diretor });
            }

            _context.Atores.AddRange(atores);
            _context.Generos.AddRange(generos);
            _context.Diretores.AddRange(diretores);

            _context.Filmes.Add(filme);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFilme", new { id = filme.Id }, filme);
        }
    }
}
